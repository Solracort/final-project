import { PokemonRepository } from "../repositories/pokemon.repository";

export class DeletePokemonUseCase {
  static async execute(pokemonList, pokemonId) {
    const repository = new PokemonRepository();
    const responsedelete = (await repository.deletePokemon(pokemonId))===200;
    const index = pokemonList.findIndex(pokemon => pokemon.id === pokemonId);
    if (responsedelete && index>=0) {
        return pokemonList.filter((pokemon, i) => i !== index);
    }
    return pokemonList;
  }
}
