import { MyObjectPokemon } from "../model/pokemonmodel";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class UpdatePokemonListUseCase {
    static async execute(pokemonlist=[], pokemon) {
        const repository = new PokemonRepository();
        const pokemonUpdated =  await repository.updatePokemonList(pokemon);
        const list2return = pokemonlist.map((item,index) => 
            (index+1) === pokemon.id ? (item = pokemonUpdated) : (item = item)
        );
        return  list2return;
    }
}