import { MyObjectPokemon } from "../model/pokemonmodel";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class AllPokemonUseCase {

    static async execute(name="") {
        const repository = new PokemonRepository();

        const mypokemon =  await repository.getAllPokemon(name);
        return mypokemon.map ((item,index) => new MyObjectPokemon({
            id: index+1 , 
            name: item.name, 
            url: item.url
        }))
    }
}