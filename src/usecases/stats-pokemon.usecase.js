import { PokemonRepository } from "../repositories/pokemon.repository";


export class StatsPokemonUseCase {
  static async execute(url) {
    const repository = new PokemonRepository();
    const responseStats = await repository.getPokemonStats(url)
    
    return responseStats;
  }
}
