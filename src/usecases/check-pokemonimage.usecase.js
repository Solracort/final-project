import { MyObjectPokemon } from "../model/pokemonmodel";
import { PokemonRepository } from "../repositories/pokemon.repository";

export class CheckPokemonImageUseCase {

    static async execute(id) {
        const repository = new PokemonRepository();
        const mypokemonimage =  await repository.getPokemonImageById(id);

        return mypokemonimage; 
    }
}