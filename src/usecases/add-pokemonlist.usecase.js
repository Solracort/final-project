import { PokemonRepository } from "../repositories/pokemon.repository";

export class AddPokemonListUseCase {
    static async execute(pokemonlist, pokemon) {
        const repository = new PokemonRepository();
        const pokemon2add =  await repository.addPokemon(pokemon);
        function checkInsertion (pokemonlist, pokemon2add){
            const isIn = pokemonlist.some(item=> item.id === pokemon2add.id);
            if (!isIn) {
                pokemonlist.push(pokemon2add);
            }
            return pokemonlist;
        }
        const list2return= checkInsertion(pokemonlist,pokemon2add);
        return  list2return;
    }
}