import axios from "axios";

export class PokemonRepository {
  async getAllPokemon(name="") {
    try{
      return (await axios.get("https://pokeapi.co/api/v2/pokemon/?limit=60")).data.results;
    }catch (error) {
      console.error(error);
    }    
  }
  async getPokemonStats(url){
    try{
      return (await axios.get(`${url}`)).data;
    } catch (error){
      console.error(error);
    }
  }
  async getPokemonImageById(identity) {
    try {
      if(identity !== ""){
        const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${identity}`);
        const imageUrl = response.data.sprites.other['official-artwork'].front_default;
        return imageUrl;
      } else {
        return "#";
      }
    } catch (error) {
      console.error(error);
    }
  }
  async updatePokemonList(pokemonChanged) {
    try {
      const pokemonApi = {
        name: pokemonChanged.name,
        url: pokemonChanged.url
      }
      // This line below could be the call to Pokemon API, the problem is that API doesnt have 
      // the put method as a valid one and return an error:
      // const response = await axios.put(`https://pokeapi.co/api/v2/pokemon/${pokemonChanged.id}`,pokemonApi);
      const responsePut = pokemonApi;   
      return responsePut;
    } catch (error) {
      console.error(error);
    }
  }
  async addPokemon(newpokemon) {
    try {
      const pokemonApi = {
        name: newpokemon.name,
        url: newpokemon.url
      }
      // This line below could be the call to Pokemon API, the problem is that API doesnt have 
      // the put method as a valid one and return an error:
      // const response = await axios.put(`https://pokeapi.co/api/v2/pokemon/${newpokemon.id}`,pokemonApi);
      const responsePut = {
        id : newpokemon.id,
        name: pokemonApi.name,
        url: pokemonApi.url
      }; 
      return responsePut;
    } catch (error) {
      console.error(error);
    }
  }
  async deletePokemon(pokemonId) {
    try{
      // This line below could be the call to Pokemon API, the problem is that API doesnt have 
      // the delete method as a valid one and return an error:
      // return await axios.delete(`https://pokeapi.co/api/v2/pokemon/${id}/`);
      return 200;
    }  catch (error) {
      console.error(error);
    }
  }
}
