import { LitElement, html } from "lit";
import { DeletePokemonUseCase } from "../usecases/delete-pokemon.usecase";
import { AddPokemonListUseCase } from "../usecases/add-pokemonlist.usecase";
import { MyObjectPokemon } from "../model/pokemonmodel";

export class PokemonFavsComponent extends LitElement {
  static get properties() {
    return {
      pokemonCollection : {type: Array},
    };
  };
  constructor() {
    super();
    this.pokemonCollection = [];
  }
  connectedCallback() {
    super.connectedCallback();
    const updateButton = document.querySelector('#update');
    updateButton.removeEventListener('click', this._updateButtonClickHandler);
    updateButton.addEventListener('click', this._updateButtonClickHandler);
    const deleteButton = document.querySelector('#delete');
    deleteButton.removeEventListener('click', this._deleteButtonClickHandler);
    deleteButton.addEventListener('click', this._deleteButtonClickHandler);
  }
  _updateButtonClickHandler = async () => {
      const idinput = document.querySelector("#idPokemon");
      const nameinput = document.querySelector("#name");
      const urlinput = document.querySelector("#url");
      const newPokemon = new MyObjectPokemon({
        id: idinput.value, 
        name: nameinput.value, 
        url: urlinput.value
      });
      (this.pokemonCollection.length<5)?this.pokemonCollection= await AddPokemonListUseCase.execute(this.pokemonCollection, newPokemon)
      :console.log(this.pokemonCollection);
      this.requestUpdate();
  };
  _deleteButtonClickHandler = async () => {
    const idinput = document.querySelector("#idPokemon");
    const nameinput = document.querySelector("#name");
    const urlinput = document.querySelector("#url");
    const newPokemon = new MyObjectPokemon({
      id: idinput.value, 
      name: nameinput.value, 
      url: urlinput.value
    });
    this.pokemonCollection= await DeletePokemonUseCase.execute(this.pokemonCollection, newPokemon.id);
    console.log(this.pokemonCollection);
    this.requestUpdate();
};
  render() {
    return html`
            <div id= "favs">
              <h2>My favorites Pokemons (Max. 5): </h2>
              ${this.pokemonCollection.map(item => {
                return html `<span class="favitem">${item.name}</span>`;
              })}
            </div>  
    `;
  }
  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-favs", PokemonFavsComponent);
export default PokemonFavsComponent;

