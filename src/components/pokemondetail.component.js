import { LitElement, html } from "lit";
import { CheckPokemonImageUseCase } from "../usecases/check-pokemonimage.usecase";
import { StatsPokemonUseCase } from "../usecases/stats-pokemon.usecase";
import "./pokemonstats.component";

export class PokemonDetailComponent extends LitElement {
  
  static get properties() {
    return {
      inputid: {type: Object},
      inputname: {type: Object},
      inputurl: {type:Object},
    };
  };
  connectedCallback() {
    super.connectedCallback();
  };
  
  render() {
    return html`
      <div id="myselectedpokemon">
        <h1 id="detailTitle">Pokemon Detail</h1>
        <div id="choice">
          <div id="field5">
            <pokemon-stats></pokemon-stats>
          </div>
          <div id="field1">
            <label for="idPokemon">Id: </label>
            <input name="idPokemon" type="text" id="idPokemon" />
          </div>
          <div id="field2">
            <label for="title">Name: </label>
            <input name="title" type="text" id="name" />
          </div>
          <div id="field3">
            <label for="content">URL: </label>
            <input name="content" type="text" id="url" />
          </div>
        </div>
      </div>
    `;
  }
  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-detail", PokemonDetailComponent);
export default PokemonDetailComponent;