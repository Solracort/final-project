import { LitElement, html } from "lit";
import { DeletePokemonUseCase } from "../usecases/delete-pokemon.usecase";
import { CheckPokemonImageUseCase } from "../usecases/check-pokemonimage.usecase";
import { StatsPokemonUseCase } from "../usecases/stats-pokemon.usecase";
import "./pokemonfavs.component";

export class PokemonStatsComponent extends LitElement {
  static get properties() {
    return {
      inputid: {type: Object},
      inputname: {type: Object},
      inputurl: {type:Object},
      imageUrl: { type: String },
      pokemonData : {type: Object},
      showPic : {type: Object}
    };
  };
  constructor() {
    super();
    this.imageUrl="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png";
    this.pokemonData ={};
  }
  connectedCallback() {
    super.connectedCallback();
    this.inputid = document.querySelector('#idPokemon');
    this.inputname= document.querySelector('#name');
    this.inputurl = document.querySelector('#url');
    const detailButton = document.querySelector('#titlelist');
    detailButton.addEventListener("click", async ()=>{
      await this.showPic(this.inputid.value);
    })
  };
  async showPic(id) {
    this.imageUrl = await CheckPokemonImageUseCase.execute(id);
    await this.showStats(this.inputurl.value)
  };
  async showStats(url){
    this.pokemonData= await StatsPokemonUseCase.execute(url);
  };
  cancelAction(){
    this.inputid.value="";
    this.inputname.value="";
    this.inputurl.value="";
    this.imageUrl="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png";
    this.pokemonData ={};
  }
  render() {
    
    return html`
        <div id="statscard">
          <div id="titlecard">
            <p id="nameshow">${(this.inputname.value).toUpperCase()}</p>
          </div>  
          <div id="contentcard">
            <div id= "picture">
              <img width="50%" src=${this.imageUrl } />
            </div>
          
            <div id="datasheet">
              <p><span>Weight:  </span><span class="spec">${this.pokemonData.weight}</span></p>
              <p><span>Height: </span><span class="spec">${this.pokemonData.height}</span></p>
              <p><span>Exp: </span><span class="spec">${this.pokemonData.base_experience}</span></p>
            </div>
          </div>  
        </div> 
        <div id="actions">
                <button id="cancel" @click=${()=>this.cancelAction()}>Cancel</button>
                <button id="update">Add Favorites</button>
                <button id="delete">Delete Favorites</button>
        </div>  
        <pokemon-favs></pokemon-favs>     
    `;
  }
  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-stats", PokemonStatsComponent);
export default PokemonStatsComponent;