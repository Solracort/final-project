import { LitElement, html } from "lit";
import { AllPokemonUseCase } from "../usecases/all-pokemon.usecase";
import "./pokemondetail.component";

export class PokemonlistComponent extends LitElement {
  
  static get properties() {
    return {
      pokemons: { type: Array },
      idinput : {type: Object},
      nameinput: { type: Object },
      urlinput: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.pokemons = await AllPokemonUseCase.execute();
   
    this.myPokemonList = this.pokemons;
    this.idinput = document.querySelector('#idPokemon');    
    this.nameinput = document.querySelector('#name');
    this.urlinput = document.querySelector('#url'); 
  }
  updateInput(name , url, idPokemon) {
    this.idinput.value = idPokemon;
    this.nameinput.value = name;
    this.urlinput.value = url;
  }; 
  render() {
    return html`
      <div class = "container">
          <div id="myFullPokemon">
              <h2>Top Pokemon List </h2><span>(Select One):</span>
                  <ul id="titlelist">
                    ${this.myPokemonList?.map(
                      (item,key) => html`<li id="${key}"   @click='${()=>this.updateInput(item.name,item.url,(key+1))}'>
                      ${item.name}
                      </li>`
                    )}
                  </ul>
          </div>
          <div id="detailview">
            <pokemon-detail></pokemon-detail>
          </div>
      </div>     
    `;
  }
  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-list", PokemonlistComponent );
