import "./main.css";
import { Router } from "@vaadin/router";
import "./pages/home.page.js";
import "./pages/pokemon.page";

console.log("Vanilla app with LitElement");

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/pokemon", component: "pokemon-page" },
  { path: "(.*)", redirect: "/" },
]);
