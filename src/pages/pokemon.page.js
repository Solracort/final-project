import "../components/pokemonlist.component"

export class PokemonPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <pokemon-list></pokemon-list>
    `;
  }
}
customElements.define("pokemon-page", PokemonPage);
