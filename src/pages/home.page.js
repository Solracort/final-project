export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <section id="homesection">
        <p>This is My Pokemon home page. Building... </p>
        <img width="65%" src = "https://assetsio.reedpopcdn.com/trucos-pokemon-go-1467896753658.jpg?width=690&quality=75&format=jpg&auto=webp" Alt="pokemon Logo"/>
      </section>`;
  }
}

customElements.define("home-page", HomePage);
