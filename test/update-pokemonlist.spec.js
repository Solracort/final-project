import { MyObjectPokemon } from "../src/model/pokemonmodel";
import { UpdatePokemonListUseCase } from "../src/usecases/update-pokemonlist.usecase";
import { PokemonFixture } from "./fixtures/pokemons";
describe('Update Pokemon List', () => {
    it ('returns an updated Pokemon List', async () => {
      
        const Pokemon = new MyObjectPokemon({
            id: 1,
            name : 'Nice New pokemon Name',
            url: 'https://pokeapi.co/api/v2/pokemon/1/'
        })
        const newPokemonList = await UpdatePokemonListUseCase.execute(PokemonFixture,Pokemon);
      
      expect(newPokemonList.length).toBe(60);
      expect(newPokemonList[0].name).toBe(Pokemon.name);
    });
  });