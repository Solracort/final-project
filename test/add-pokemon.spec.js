import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { AddPokemonListUseCase } from "../src/usecases/add-pokemonlist.usecase";
import { PokemonFixture } from "./fixtures/pokemons";

jest.mock("../src/repositories/pokemon.repository");
describe('All pokemons use case', ()=>{
    beforeEach(()=>{
        PokemonRepository.mockClear();
    })
    it ("should get add a pokemon to a list", async ()=>{
        PokemonRepository.mockImplementation(()=>{
            return {
                addPokemon: (pokemon) => {
                    return {
                        id : 400,
                        name: "generationVII",
                        url: "https://carlos-final-project.web.app/"
                      };
                }
            }
        });
        const pokemonlist = [
            {
                id:1,
                name: "ballena",
                url: "ballena.com"
            },
            {
                id:2,
                name: "ballenato",
                url: "ballenato.com"
            }
        ];

        const newpokemon = {
            id: 400,
            name: "generationVII",
            url: "https://carlos-final-project.web.app/"
          };
        const newpokemonList = await AddPokemonListUseCase.execute(pokemonlist, newpokemon);
        
        
        expect(newpokemonList.length).toBe(3);

    });
});