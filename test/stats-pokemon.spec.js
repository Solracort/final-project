import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { AllPokemonUseCase } from "../src/usecases/all-pokemon.usecase";
import { StatsPokemonUseCase } from "../src/usecases/stats-pokemon.usecase";
import { PokemonFixture } from "./fixtures/pokemons";

jest.mock("../src/repositories/pokemon.repository");
describe('All pokemons use case', ()=>{
    beforeEach(()=>{
        PokemonRepository.mockClear();
    })
    it ("should get all pokemon list", async ()=>{
        PokemonRepository.mockImplementation(()=>{
            return {
                getPokemonStats: (url) => {
                    return {
                        weight: 50,
                        height: 6,
                        experience: 117
                    };
                }
            }
        });
        const url = "https://pokeapi.co/api/v2/pokemon/5/"
        const pokemonStats = await StatsPokemonUseCase.execute(url);

        
        expect(pokemonStats.weight).toBe(50);

    });
});