import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { DeletePokemonUseCase } from "../src/usecases/delete-pokemon.usecase";
import { PokemonFixture } from "./fixtures/pokemons";

jest.mock("../src/repositories/pokemon.repository");
describe('Delete Pokemon List', () => {
    beforeEach(() => {
      PokemonRepository.mockClear();
    });
    it ('should delete a pokemon', async () => {
      PokemonRepository.mockImplementation(() => {
        return {
          deletePokemon: () => 200,
        };
      });
      let PokemonList2test = [];
      let i=0;
      while (i<PokemonFixture.length){
          PokemonList2test[i]= {
            id: i+1,
            name: PokemonFixture[i].name,
            url: PokemonFixture[i].url
          };
          i++;
      }
     
      const pokemonId = 1;
      const newPokemonList = await DeletePokemonUseCase.execute(PokemonList2test, pokemonId);
      expect(newPokemonList.length).toBe(PokemonFixture.length-1);
      
    });
  });