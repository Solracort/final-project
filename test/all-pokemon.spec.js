import { PokemonRepository } from "../src/repositories/pokemon.repository";
import { AllPokemonUseCase } from "../src/usecases/all-pokemon.usecase";
import { PokemonFixture } from "./fixtures/pokemons";

jest.mock("../src/repositories/pokemon.repository");
describe('All pokemons use case', ()=>{
    beforeEach(()=>{
        PokemonRepository.mockClear();
    })
    it ("should get all pokemon list", async ()=>{
        PokemonRepository.mockImplementation(()=>{
            return {
                getAllPokemon: () => {
                    return PokemonFixture;
                }
            }
        })
        const pokemonList = await AllPokemonUseCase.execute();
        // console.log("🚀 ~ file: all-pokemon.spec.js:7 ~ it ~ pokemonList:", pokemonList)
        
        expect(pokemonList.length).toBe(60);

    });
});