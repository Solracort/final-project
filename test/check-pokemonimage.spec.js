import { CheckPokemonImageUseCase } from "../src/usecases/check-pokemonimage.usecase";
describe('getPokemonImageById', () => {
    it ('returns a valid image URL', async () => {
      
        const imageUrl = await CheckPokemonImageUseCase.execute(1);
      
      expect(imageUrl).toMatch(/^https?:\/\/.*\.(png|jpg|jpeg)$/);
    });
  });