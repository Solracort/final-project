/// <reference types="Cypress"/>

describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/');
    cy.get('#navPokemon').click();
    cy.get('#3').click();
    cy.get('#begin').click();
    cy.get('#cancel').click();
    cy.get('#4').click();
    cy.get('#begin').click();
    cy.get('#update').click();
    cy.get('#1').click();
    cy.get('#begin').click();
    cy.get('#update').click();
    cy.get('#6').click();
    cy.get('#begin').click();
    cy.get('#update').click();
    cy.get('#25').click();
    cy.get('#begin').click();
    cy.get('#update').click();
    cy.get('#6').click();
    cy.get('#begin').click();
    cy.get('#delete').click();
  })
})